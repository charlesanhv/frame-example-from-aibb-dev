/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        remotePatterns: [
            {
              protocol: 'https',
              hostname: 'xscope.xyz',
              port: '',
              pathname: '/public/images/**',
            },
            {
                protocol: 'https',
                hostname: 'statistr.com',
                port: '',
                pathname: '/assets/images/**',
              },
          ],
    }
};

export default nextConfig;
