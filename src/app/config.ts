export const NEXT_PUBLIC_URL = "https://xscope.xyz";
export const NEXT_PUBLIC_URL_STATISTR = "https://statistr.com";
export const NEXT_PUBLIC_URL_FRAMES = "https://frames.statistr.com";
export const EMPTY_DATA = '--'