class ItemList {
  x: number;
  y: number;
  text: string;
  fontSize: string | number;
  fontFamily: string;
  constructor(
    x: number,
    y: number,
    text: string,
    fontSize: string | number = 30,
    fontFamily: string = "Montserrat"
  ) {
    this.x = x;
    this.y = y;
    this.text = text;
    this.fontSize = fontSize;
    this.fontFamily = fontFamily;
  }

  draw(ctx: any) {
    ctx.font = `${this.fontSize}px ${this.fontFamily}`;
    ctx.fillText(this.text, this.x, this.y);
  }
}

class ListNameCanvas {
  x: number;
  y: number;
  data: string[];
  fontSize: string | number;
  fontFamily: string;
  constructor(
    x: number,
    y: number,
    data: string[],
    fontSize: string | number = 30,
    fontFamily: string = "Montserrat"
  ) {
    this.x = x;
    this.y = y;
    this.data = data;
    this.fontSize = fontSize;
    this.fontFamily = fontFamily;
  }

  draw(ctx: any) {
    const space = Number(this.fontSize) + 3;
    this.data.forEach((name, index) => {
      const nameC = new ItemList(
        this.x,
        this.y + Number(this.fontSize) + index * space,
        name,
        this.fontSize,
        this.fontFamily
      );
      nameC.draw(ctx);
    });
  }
}

export { ListNameCanvas };
