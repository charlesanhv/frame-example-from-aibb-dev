export const TYPE_BUTTON: {
  project: string;
  people: string;
  organization: string;
} = {
  project: "project",
  people: "people",
  organization: "organization",
};
