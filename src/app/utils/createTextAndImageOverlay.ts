import { createCanvas, registerFont } from "canvas";
import sharp from "sharp";
import fs from "fs";
import path from "path";

export const createTextImageAndOverlay = async () => {
  //   const apiKeyToken = process.env.ETHERSCAN;
  //   const url = `https://api.etherscan.io/api?module=stats&action=ethprice&apikey=${apiKeyToken}`;

  try {
    //     const response = await fetch(url);
    //     if (!response.ok) {
    //       throw new Error(`Error: ${response.status}`);
    //     }
    // const data = await response.json();

    const textCurrent = `data statistr`;

    const canvas = createCanvas(1000, 1000);
    const ctx = canvas.getContext("2d");

    registerFont(path.resolve("./public/fonts/Montserrat-BoldItalic.ttf"), {
      family: "Montserrat-BoldItalic",
    });

    ctx.fillStyle = "#000000";
    ctx.font = "58px Montserrat";
    var text = ctx.measureText(textCurrent)
    const textWidth = text.width;
    ctx.strokeStyle = "rgba(0,0,0,0.5)";
    ctx.beginPath();
    ctx.lineTo(50, 102);
    ctx.lineTo(50 + text.width, 102);
    ctx.stroke()
    const textHeight = parseInt(ctx.font, 10);
    const textX = (canvas.width - textWidth) / 2;
    const textY = (canvas.height - textHeight) / 2;
    ctx.fillText(textCurrent, textX, textY + textHeight);

    const textBuffer = canvas.toBuffer("image/png");

    const ethImagePath = path.resolve("./public/background.png");

    const ethImageBuffer = fs.readFileSync(ethImagePath);

    const newImageBuffer = await sharp(ethImageBuffer)
      .composite([{ input: textBuffer }])
      .toBuffer();

    return { textCurrent, newImageBuffer };
  } catch (error) {
    console.error("Error:", error);
    const ethImagePath = path.resolve("./public/background.png");
    return { textCurrent: error, ethImagePath: ethImagePath };
  }
};
