import { EMPTY_DATA } from "../config";

export const mappingText = (data: any[] = [], value: number) => {
  return data.length > value
    ? `${data.slice(0, value).join(", ")}, ...`
    : data.join(", ") || EMPTY_DATA;
};
