type ParamsType = {
  paramsOfAPI: Array<string>;
  defaultParams?: Object;
  jsonObject?: Array<string>;
  jsonArray?: Array<string>;
};

type StructConfigType = {
  sid: string;
  name: string;
  constant?: Object;
  targetNameData: string;
  paramsOfAPI?: Array<string>;
  targetsName?: Object;
  targets?: { [key: string]: ParamsType };
  defaultParamsBuilder?: {};
  defaultParams?: {};
  getBody: Function;
  getData: Function;
};

const encodeJsonArrayObject = (
  dataArray: any = [],
  listFieldStringJson: Array<string> = []
) => {
  return dataArray.map((itemData: Object) => {
    return encodeJsonObject(itemData, listFieldStringJson);
  });
};

const encodeJsonObject = (
  dataObject: any = {},
  listFieldStringJson: Array<string> = []
) => {
  listFieldStringJson.forEach((field) => {
    if (dataObject?.hasOwnProperty(field)) {
      let dataJson = undefined;
      try {
        dataJson = JSON.parse(dataObject[field]);
        // Fillter for id of GroupBy = NULL
        if (Array.isArray(dataJson)) {
          dataJson = dataJson.filter((itemData) => itemData != null);
        }
      } catch (error) {}

      dataObject = {
        ...dataObject,
        [field]: dataJson,
      };
    }
  });
  return dataObject;
};

const findData = (
  dataFromAPI: Array<{ [key: string]: any }>,
  targetNameData = ""
) => {
  let dataOfTarget =
    dataFromAPI.find((itemData, index) => {
      return itemData.hasOwnProperty(targetNameData);
    }) || {};

  let { [targetNameData]: result = undefined } = dataOfTarget;
  return result;
};

export const framesSearchGlobal: StructConfigType = {
  sid: "664b1815dba1617cce09bec9",
  name: "frames_search_global",
  targetNameData: "frames_search_global",
  paramsOfAPI: ["type", "searchValue", "limit"],
  defaultParams: { limit: { max: 30, min: 0 } },
  getBody: function (params: Object) {
    const paramsBuild = {
      ...this.defaultParams,
      ...params,
    };
    return {
      body: {
        sid: this.sid,
        params: [
          {
            ...paramsBuild,
          },
        ],
      },
    };
  },
  getData: function (dataFromAPI: Array<any> = []) {
    const listData = findData(dataFromAPI, this.targetNameData);
    if (listData === undefined) return [];
    if (listData.length === 0) return [];
    return encodeJsonArrayObject(listData, [
      "project",
      "people",
      "organization",
    ]);
  },
};

export const framesProjectsDetails: StructConfigType = {
  sid: "664b0ead837a3851070e3260",
  name: "frames_projects_details",
  targetNameData: "frames_projects_details",
  paramsOfAPI: ["id"],
  defaultParams: { id: "" },
  getBody: function (params: Object) {
    const paramsBuild = {
      ...this.defaultParams,
      ...params,
    };
    return {
      body: {
        sid: this.sid,
        params: [
          {
            ...paramsBuild,
          },
        ],
      },
    };
  },
  getData: function (dataFromAPI: Array<any> = []) {
    const listData = findData(dataFromAPI, this.targetNameData);
    if (listData === undefined) return [];
    if (listData.length === 0) return [];
    return encodeJsonArrayObject(listData, ["related_organizations", "sector"]);
  },
};

export const framesPeopleDetails: StructConfigType = {
  sid: "664b0ead837a3851070e3261",
  name: "frames_people_details",
  targetNameData: "frames_people_details",
  paramsOfAPI: ["id"],
  defaultParams: { id: "" },
  getBody: function (params: Object) {
    const paramsBuild = {
      ...this.defaultParams,
      ...params,
    };
    return {
      body: {
        sid: this.sid,
        params: [
          {
            ...paramsBuild,
          },
        ],
      },
    };
  },
  getData: function (dataFromAPI: Array<any> = []) {
    const listData = findData(dataFromAPI, this.targetNameData);
    if (listData === undefined) return [];
    if (listData.length === 0) return [];
    return encodeJsonArrayObject(listData, [
      "organization",
      "portfolio_angel_investor",
      "relate_token",
      "role",
    ]);
  },
};

export const framesOrganizationsDetails: StructConfigType = {
  sid: "664b0ead837a3851070e3262",
  name: "frames_organizations_details",
  targetNameData: "frames_organizations_details",
  paramsOfAPI: ["id"],
  defaultParams: { id: "" },
  getBody: function (params: Object) {
    const paramsBuild = {
      ...this.defaultParams,
      ...params,
    };
    return {
      body: {
        sid: this.sid,
        params: [
          {
            ...paramsBuild,
          },
        ],
      },
    };
  },
  getData: function (dataFromAPI: Array<any> = []) {
    const listData = findData(dataFromAPI, this.targetNameData);
    if (listData === undefined) return [];
    if (listData.length === 0) return [];
    return encodeJsonArrayObject(listData, [
      "related_organizations",
      "sector",
      "relate_project",
      'employees',
      'fundraising_projects',
      'founder'
    ]);
  },
};
