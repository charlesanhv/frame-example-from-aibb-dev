interface IGetDataStruct {
  body: any;
}

const base_url = "https://statistr.com/api/data/struct";

export async function getDataStruct({ body }: IGetDataStruct) {
  const { sid, params } = body;

  const encodedParams = encodeURIComponent(JSON.stringify(params));

  const paramsString = `sid=${sid}&params=${encodedParams}`;

  try {
    const res = await fetch(`${base_url}?${paramsString}`);

    const data = await res.json();

    return data;
  } catch (error) {
    console.error("error", error);
  }
}
