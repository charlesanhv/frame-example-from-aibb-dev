/* eslint-disable @next/next/no-img-element */
/* eslint-disable react/jsx-key */
import { frames } from "../frames";
import { Button } from "frames.js/next";
import { createTextImageAndOverlay } from "@/app/utils/createTextAndImageOverlay";

export const POST = frames(async (ctx) => {
  const { textCurrent, newImageBuffer } = await createTextImageAndOverlay();
  const base64Image =
    (newImageBuffer && newImageBuffer.toString("base64")) || "";
  const dataUrl = `data:image/png;base64,${base64Image}`;
  return {
    image: (
      // <div
      //   tw="flex"
      //   style={{
      //     backgroundImage:
      //       "url('${NEXT_PUBLIC_URL}/_next/static/media/XScope_Eff.7849bdca.svg')",
      //     width: "100%",
      //     height: "100%",
      //   }}
      // >
      //   Route 3
      // </div>
      <img
        src={dataUrl}
        alt="image"
      />
    ),
    buttons: [
      // <Button action="post" target="https://frames.xsope.xyz/frames">
      //   Home
      // </Button>,
      <Button
        action="post"
        target={{
          protocol: "https",
          host: `${process.env.VERCEL_URL}`,
          pathname: "/",
        }}
      >
        Home
      </Button>,
      <Button action="link" target="https://statistr.com">
        Open Statistr
      </Button>,
    ],
  };
});
