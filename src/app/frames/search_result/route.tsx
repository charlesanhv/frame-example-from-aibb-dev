/* eslint-disable react/jsx-key */
import { frames } from "../frames";
import { Button } from "frames.js/next";
import { getDataStruct } from "@/app/utils/getDataStruct";
import { framesSearchGlobal } from "@/app/utils/Struct";

const handler = frames(async (ctx) => {
  let state = ctx.state;

  switch (ctx.searchParams?.action) {
    case "increment":
      state = { ...state, page: state.page + 1 };
      break;
    case "decrement":
      state = { ...state, page: state.page - 1 };
      break;
  }

  const { type = "project" } = ctx.searchParams;
  const inputText = ctx.message?.inputText;

  const defaultDataParams = {
    ...framesSearchGlobal.defaultParams,
    type,
    searchValue: inputText,
  };

  const data = framesSearchGlobal
    .getData(
      await getDataStruct({
        body: framesSearchGlobal?.getBody(defaultDataParams).body,
      })
    )
    .map((item: any) => item[type]);

  const dataDisplay = data.slice(
    (state.page - 1) * 15,
    (state.page - 1) * 15 + 15
  );

  const isDisableNext = data.length <= state.page * 15;
  const isDiablePrev = state.page === 1;

  return {
    image: (
      <div
        tw="flex w-full h-full bg-black text-white p-[50px] pl-[100px]"
        style={{
          flexDirection: "column",
        }}
      >
        {(dataDisplay || [])?.map((item: any, index: number) => (
          <div tw="mb-[20px]" key={index}>{`${item?.id} - ${item?.name}`}</div>
        ))}
      </div>
    ),
    imageOptions: {
      aspectRatio: "1:1",
    },
    textInput: "Enter id number...",
    buttons: [
      !isDiablePrev ? (
        <Button
          action="post"
          target={{
            protocol: "https",
            host: `${process.env.VERCEL_URL}`,
            pathname: "/search_result",
            query: { action: "decrement" },
          }}
        >
          Prev
        </Button>
      ) : (
        <Button
          action="post"
          target={{
            protocol: "https",
            host: `${process.env.VERCEL_URL}`,
            pathname: "/",
          }}
        >
          Home
        </Button>
      ),

      !isDisableNext && (
        <Button
          action="post"
          target={{
            protocol: "https",
            host: `${process.env.VERCEL_URL}`,
            pathname: "/search_result",
            query: { action: "increment" },
          }}
        >
          Next
        </Button>
      ),

      <Button
        action="post"
        target={{
          protocol: "https",
          host: `${process.env.VERCEL_URL}`,
          pathname: "/get_detail",
          query: { type },
        }}
      >
        Get detail
      </Button>,
    ],
    state,
  };
});

export const GET = handler;
export const POST = handler;
