import { createFrames } from "frames.js/next";

export type State = {
  page: number;
};

export const frames = createFrames<State>({
  basePath: "/frames",
  initialState: {
    page: 1,
  },
});
