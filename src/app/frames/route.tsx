/* eslint-disable react/jsx-key */
import { Button } from "frames.js/next";
import { frames } from "./frames";
import { TYPE_BUTTON } from "../utils/constant";

import { NEXT_PUBLIC_URL_FRAMES } from "../config";

const handleRequest = frames(async (ctx) => {
  // const btnIndex = ctx.message?.buttonIndex;
  // const inputText = ctx.message?.inputText;

  // let apiData = null;

  // if (btnIndex === 1 && inputText) {
  //   const response = await fetch(
  //     `${NEXT_PUBLIC_URL}/ai-api/simple-report?twitter_link=https://twitter.com/${inputText}`
  //   );
  //   if (response.ok) {
  //     apiData = await response.json();
  //   }
  // }

  return {
    // image: !apiData ? (
    //   `${NEXT_PUBLIC_URL_STATISTR}/default-og.jpg`
    // ) : (
    //   <div
    //     style={{
    //       fontSize: "30px",
    //       textAlign: "center",
    //       padding: "20px",
    //     }}
    //     tw="flex w-full h-full bg-black text-white"
    //   >
    //     {apiData?.response}
    //   </div>
    // ),
    image: `${NEXT_PUBLIC_URL_FRAMES}/background.jpg`,
    imageOptions: {
      aspectRatio: "1:1",
    },

    textInput: "Enter name...",

    buttons: [
      // test on local
      <Button
        action="post"
        target={{
          // protocol: "https",
          // host: `${process.env.VERCEL_URL}`,
          pathname: "/search_result",
          query: {
            type: TYPE_BUTTON.project,
          },
        }}
      >
        Project 📚
      </Button>,

// test on domain before deploy
      // <Button
      //   action="post"
      //   target={{
      //     protocol: "https",
      //     host: `${process.env.VERCEL_URL}`,
      //     pathname: "/search_result",
      //     query: {
      //       type: TYPE_BUTTON.project,
      //     },
      //   }}
      // >
      //   Project 📚
      // </Button>,

      <Button
        action="post"
        target={{
          protocol: "https",
          host: `${process.env.VERCEL_URL}`,
          pathname: "/search_result",
          query: {
            type: TYPE_BUTTON.people,
          },
        }}
      >
        People 👨‍👦‍👦
      </Button>,

      <Button
        action="post"
        target={{
          protocol: "https",
          host: `${process.env.VERCEL_URL}`,
          pathname: "/search_result",
          query: {
            type: TYPE_BUTTON.organization,
          },
        }}
      >
        Organizations 📝
      </Button>,
    ],

    // report: apiData,
  };
});

export const GET = handleRequest;
export const POST = handleRequest;
