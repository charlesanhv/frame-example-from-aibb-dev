/* eslint-disable react/jsx-key */
import { frames } from "../frames";
import { Button } from "frames.js/next";
import { TYPE_BUTTON } from "@/app/utils/constant";

import {
  framesProjectsDetails,
  framesPeopleDetails,
  framesOrganizationsDetails,
} from "@/app/utils/Struct";

import { getDataStruct } from "@/app/utils/getDataStruct";
import { EMPTY_DATA } from "@/app/config";
import { mappingText } from "@/app/utils";
// import { createTextImageAndOverlay } from "@/app/utils/createTextAndImageOverlay";

const API_STRUCT: any = {
  project: framesProjectsDetails,
  people: framesPeopleDetails,
  organization: framesOrganizationsDetails,
};

const ITEMS_PER_PAGE = 10;

export const POST = frames(async (ctx) => {
  const { type = "project" } = ctx.searchParams;
  const inputText = ctx.message?.inputText;

  const defaultDataParams = {
    ...API_STRUCT[type].defaultParams,
    id: inputText,
  };

  const data = await getDataStruct({
    body: API_STRUCT[type]?.getBody(defaultDataParams).body,
  });

  const dataDisplay = API_STRUCT[type].getData(data)[0];

  // const text: string | any = `Name: ${data?.Name}
  //     Summary: ${data?.Summary}
  //     Location: ${data?.Location}
  //     Project Website: ${data?.ProjectWebsite}
  //     Price: ${data?.Price}
  //     Market Cap: ${data?.Marketcap}
  //     Team Member: ${data?.TeamMember}
  //     Sector: ${data?.sector.join?.(", ")}`;

  // const { textCurrent, newImageBuffer } = await createTextImageAndOverlay(text);
  // const base64Image =
  //   (newImageBuffer && newImageBuffer.toString("base64")) || "";

  // const dataUrl = `data:image/png;base64,${base64Image}`;
  console.log("dataDisplay", dataDisplay);
  return {
    image:
      type === TYPE_BUTTON.project ? (
        <div
          tw="flex w-full h-full bg-black text-white p-[50px] pl-[100px]"
          style={{
            flexDirection: "column",
            fontSize: "32px",
          }}
        >
          <div
            tw="mb-[20px]"
            style={{
              display: "flex",
              fontSize: "50px",
              justifyContent: "center",
              textTransform: "capitalize",
            }}
          >
            {type}
          </div>
          <div tw="mb-[20px]">{`Name: ${dataDisplay?.name || EMPTY_DATA}`}</div>
          <div tw="mb-[20px]">{`Location: ${
            dataDisplay?.location || EMPTY_DATA
          }`}</div>
          <div tw="mb-[20px]">{`Project Website: ${
            dataDisplay?.website || EMPTY_DATA
          }`}</div>
          <div tw="mb-[20px]">{`Price: ${
            dataDisplay?.price_usd || EMPTY_DATA
          }`}</div>
          <div tw="mb-[20px]">{`Marketcap: ${
            dataDisplay?.market_cap_usd || EMPTY_DATA
          }`}</div>
          <div tw="mb-[20px]">{`Team Member: ${
            mappingText(dataDisplay?.all_member || [], 5)
            // dataDisplay?.all_member?.join?.(", ") || EMPTY_DATA
          }`}</div>

          <div tw="mb-[20px]">{`Founder: ${
            mappingText(dataDisplay?.founder || [], 5)
            // dataDisplay?.founder?.join?.(", ") || EMPTY_DATA
          }`}</div>
          <div tw="mb-[20px]">{`Members: ${
            mappingText(dataDisplay?.members || [], 5)
            // dataDisplay?.members?.join?.(", ") || EMPTY_DATA
          }`}</div>
          <div tw="mb-[20px]">{`Related Organizations: ${
            mappingText(dataDisplay?.related_organizations || [], 5)
            // dataDisplay?.related_organizations?.join?.(", ") || EMPTY_DATA
          }`}</div>
          <div tw="mb-[20px]">{`Sector: ${
            mappingText(dataDisplay?.sector || [], 5)
            // dataDisplay?.sector?.join?.(", ") || EMPTY_DATA
          }`}</div>
        </div>
      ) : type === TYPE_BUTTON.people ? (
        <div
          tw="flex w-full h-full bg-black text-white p-[50px] pl-[100px]"
          style={{
            flexDirection: "column",
            fontSize: "32px",
          }}
        >
          <div
            tw="mb-[20px]"
            style={{
              display: "flex",
              fontSize: "50px",
              justifyContent: "center",
              textTransform: "capitalize",
            }}
          >
            {type}
          </div>
          <div tw="mb-[20px]">{`Name: ${dataDisplay?.name || EMPTY_DATA}`}</div>
          <div tw="mb-[20px]">{`Gender: ${
            dataDisplay?.gender || EMPTY_DATA
          }`}</div>
          <div tw="mb-[20px]">{`Location: ${
            dataDisplay?.location || EMPTY_DATA
          }`}</div>
          <div tw="mb-[20px]">{`Role: ${dataDisplay?.role || EMPTY_DATA}`}</div>
          <div tw="mb-[20px]">{`Primary Organization: ${
            mappingText(dataDisplay?.organization || [], 5)
            // dataDisplay?.organization?.join?.(", ") || EMPTY_DATA
          }`}</div>
          <div tw="mb-[20px]">{`Relate project: ${
            mappingText(dataDisplay?.relate_token, 5)
            // dataDisplay?.relate_token?.join?.(", ") || EMPTY_DATA
          }`}</div>
          <div tw="mb-[20px]">{`Relate Organization:  ${
            mappingText(
              dataDisplay?.organization?.concat?.(
                dataDisplay?.portfolio_angel_investor || []
              ),
              5
            )
            // dataDisplay?.organization
            //   ?.concat?.(dataDisplay?.portfolio_angel_investor || [])
            //   ?.join?.(", ") || EMPTY_DATA
          }`}</div>
          <div tw="mb-[20px]">{`Personal Investment: ${
            dataDisplay?.personal_investment || EMPTY_DATA
          }`}</div>
          <div tw="mb-[20px]">{`Linkedin Profile: ${
            dataDisplay?.linkedin || EMPTY_DATA
          }`}</div>
        </div>
      ) : (
        <div
          tw="flex w-full h-full bg-black text-white p-[50px] pl-[100px]"
          style={{
            flexDirection: "column",
            fontSize: "32px",
          }}
        >
          <div
            tw="mb-[20px]"
            style={{
              display: "flex",
              fontSize: "50px",
              justifyContent: "center",
              textTransform: "capitalize",
            }}
          >
            {type}
          </div>
          <div tw="mb-[20px]">{`Name: ${dataDisplay?.name || EMPTY_DATA}`}</div>
          <div tw="mb-[20px]">{`Summary: ${
            dataDisplay?.summary || EMPTY_DATA
          }`}</div>
          <div tw="mb-[20px]">{`Website: ${
            dataDisplay?.website || EMPTY_DATA
          }`}</div>
          <div tw="mb-[20px]">{`Venture Type: ${
            dataDisplay?.type || EMPTY_DATA
          }`}</div>
          <div tw="mb-[20px]">{`Founded Year: ${
            dataDisplay?.year_founded || EMPTY_DATA
          }`}</div>
          <div tw="mb-[20px]">{`Location: ${
            dataDisplay?.location || EMPTY_DATA
          }`}</div>
          <div tw="mb-[20px]">{`Team Member: ${
            mappingText(
              dataDisplay?.founder?.concat?.(dataDisplay?.employees || []) || [],
              5
            )
            // dataDisplay?.founder
            //   ?.slice(0, 5)
            //   ?.concat?.(dataDisplay?.employees?.slice(0, 5) || [])
            //   .join?.(", ") || EMPTY_DATA
          }`}</div>
          <div tw="mb-[20px]">{`Number of Investment: ${
            dataDisplay?.number_of_investment || EMPTY_DATA
          }`}</div>
          <div tw="mb-[20px]">{`Total Funding Amount: ${
            dataDisplay?.total_funding_amount || EMPTY_DATA
          }`}</div>
          <div tw="mb-[20px]">{`Related Project: ${
            mappingText(dataDisplay?.relate_project || [], 5)
            // (dataDisplay?.relate_project || []).slice(0, 5).join?.(", ") || EMPTY_DATA
          }`}</div>
          <div tw="mb-[20px]">{`Related People: ${
            mappingText(
              dataDisplay?.founder
                ?.concat?.(dataDisplay?.employees || [])
                ?.concat?.(dataDisplay?.angel || []) || [],
              5
            )
            // (
            //   dataDisplay?.founder
            //     ?.concat?.(dataDisplay?.employees || [])
            //     ?.concat?.(dataDisplay?.angel || [])
            //     ?.slice(0, 5)
            //     .join?.(", ") || EMPTY_DATA
            // )
          }`}</div>
          <div tw="mb-[20px]">{`Finance: ${
            dataDisplay?.finance || EMPTY_DATA
          }`}</div>
          <div tw="mb-[20px]">{`Link Social : ${[
            dataDisplay?.twitter,
            dataDisplay?.linkedin,
            dataDisplay?.crunchbase_link,
          ]
            .filter((item) => item)
            .join(" | ")}`}</div>
        </div>
      ),
    imageOptions: {
      aspectRatio: "1:1",
    },
    buttons: [
      <Button
        action="post"
        target={{
          protocol: "https",
          host: `${process.env.VERCEL_URL}`,
          pathname: "/",
        }}
      >
        Home 🏡
      </Button>,
      <Button action="link" target="https://statistr.com">
        More info by Statistr
      </Button>,
    ],
  };
});
