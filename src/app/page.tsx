import { fetchMetadata } from "frames.js/next";

export async function generateMetadata() {
  return {
    title: "Statistr Frames",
    description: "Statistr Frames pages",
    other: await fetchMetadata(
      new URL(
        "/frames",
        !process.env.VERCEL_URL
          ? `https://${process.env.VERCEL_URL}`
          : "http://localhost:3002"
      )
    ),
  };
}

export default function Page() {
  return (
    <div
    style={{
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    }}
  >
    <h1>
      Follow{" "}
      <a
        href="https://statistr.com"
        style={{
          color: "blue",
          textDecoration: "underline",
          cursor: "pointer",
          fontWeight: "bold",
          fontSize: "20px",
        }}
      >
        @Statistr
      </a>
    </h1>
  </div>
  );
}
