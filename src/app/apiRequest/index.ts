export const searchItems = async (query: string) => {
  const response = await fetch(`/api/search?q=${query}`);
  return response.json();
};
