import { useEffect, useRef } from "react";

const useTextToImage = (
  text: string,
  width = 500,
  height = 200,
  font = "30px Arial",
  textColor = "red",
  backgroundColor = "white"
): React.MutableRefObject<HTMLCanvasElement | null> => {
  const canvasRef = useRef<HTMLCanvasElement>(null);

  useEffect(() => {
    const canvas = canvasRef.current;
    if (!canvas) return;

    const ctx = canvas.getContext("2d");
    if (!ctx) return;

    // Đặt kích thước cho canvas
    canvas.width = width;
    canvas.height = height;

    // Vẽ nền
    ctx.fillStyle = backgroundColor;
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    // Vẽ chữ
    ctx.fillStyle = textColor;
    ctx.font = font;
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.fillText(text, canvas.width / 2, canvas.height / 2);
  }, [text, width, height, font, textColor, backgroundColor]);

  return canvasRef;
};

export default useTextToImage;
